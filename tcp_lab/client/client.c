#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <time.h>


int main(int argc, char** argv)
{

  int client_socket;

  client_socket = socket(AF_INET, SOCK_STREAM, 0);
  socklen_t addr_size; 

  
  struct sockaddr_in server_addr;
	server_addr.sin_family=AF_INET;
	server_addr.sin_port=htons(9002);
	server_addr.sin_addr.s_addr=INADDR_ANY;
  
  memset(server_addr.sin_zero, '\0', sizeof server_addr.sin_zero);

  addr_size = sizeof server_addr;

  if(connect(client_socket, (struct sockaddr *) &server_addr, addr_size) ==-1)
  {
    printf("Connection failed\n");
  }

  int bufsize;
  char msg[1024];
  FILE * fp = fopen(argv[1], "r");
  if(!fp)
    printf("File open error\n");
  int s =send(client_socket, argv[1], sizeof(argv[1]), 0);
  printf("file name sent %s\n",argv[1]);

  printf("sending file\n");
  while(bufsize=fread(msg,sizeof(msg), 1, fp) > 0)
  {

    int s =send(client_socket, msg, sizeof(msg), 0);
    
    if(s<0)
    {
      printf("sending failed");
      break;
    }
    printf("in progress\n");
  }

  printf("sent the file\n");
  close(client_socket);
  fclose(fp);
  return 0;
}
