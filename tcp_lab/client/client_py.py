import socket
import sys
import time

def main():
    host = '127.0.0.1'
    port = 12345
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    file_name = sys.argv[1]

    fp = open(file_name, 'r')
    lines = fp.readlines()
    client_socket.connect((host, port))

    print("sent file name", file_name)
    client_socket.send(file_name.encode('ascii'))
    time.sleep(1)

    for line in lines:
        client_socket.send(line.encode('ascii'))

    client_socket.send("###".encode('ascii'))
    print("sent", file_name)
    print(client_socket.recv(100))

    client_socket.close()
    fp.close()


if __name__ == '__main__':
    main()
