
import socket
import os

def client_handle(client_socket, addr, i):
    """
    function to handle clients
    """
    print("in thread " + str(i))

    file_name = client_socket.recv(100)


    if not file_name:
        print("file name transfer failed")
        return

    fp = open(file_name, "w")
    data = client_socket.recv(1024).decode('ascii')
    while True:
        fp.write(data)
        #print(data)
        data = client_socket.recv(1024).decode('ascii')
        if '###' in data:
            break

    print("file transfer done\n closing thread")

    msg = "file recieved successfully"
    client_socket.send(msg.encode('ascii'))
    fp.close()
    client_socket.close()
    return



def main():
    host = ""
    port = 12345

    server_socket = socket.socket()
    server_socket.bind((host, port))

    print("socket binded to port ", port)

    server_socket.listen(50)

    print("socket is listening")

    i = 0
    while True:
        client_socket, addr = server_socket.accept()

        pid = os.fork()

        if pid < 0:
            print("failed to create a child")

        elif pid == 0:
            i+=1
            print("child handling socket")
            client_handle(client_socket, addr, i)
        else:
            os.wait()
            client_socket.close() # should this be before or after?
            print("done handling one request")

    server_socket.close()


if __name__ == "__main__":
    main()
