#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <time.h>


#define MAX_CLIENTS 3

typedef struct client_thread{

  int client_socket;
  pthread_t thread_id;
} client_thread;

void * client_handle(void * args);

int main()
{
    sigset_t new;
    sigemptyset (&new);
    sigaddset(&new, SIGPIPE);
    if (pthread_sigmask(SIG_BLOCK, &new, NULL) != 0) 
    {
        perror("Unable to mask SIGPIPE");
        exit(-1);
    }
  int server_socket, opt=1;
  server_socket = socket(AF_INET,SOCK_STREAM,0);
  struct sockaddr_storage *server_storage;
  socklen_t addr_size = sizeof(struct sockaddr_storage);

  client_thread *cl = calloc(MAX_CLIENTS,sizeof(client_thread));



  struct sockaddr_in server_addr;
	server_addr.sin_family=AF_INET;
	server_addr.sin_port=htons(9002);
	server_addr.sin_addr.s_addr=INADDR_ANY;
   

  //memset(server_addr.sin_zero, '\0', sizeof(server_addr.sin_zero));
  if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) 
    { 
        perror("setsockopt"); 
        exit(EXIT_FAILURE); 
    } 

  if(bind(server_socket, (struct sockaddr *) &server_addr, sizeof(server_addr)) == -1)
  printf("binding failed\n");
  
  // listen
  if(!listen(server_socket, MAX_CLIENTS))
  {
    printf("started to listen\n");
  }
  else
  {
    printf("error am i deff!\n");
  }

  int i =0;
  
  // for master thread*
  /*
  addr_size = sizeof server_storage;
    // new client new socket new thread
    cl[i].client_socket = accept(server_socket, (struct sockaddr *) &server_storage, &addr_size);
    cl[i].thread_id = i;

    printf("accepted connection %d\n", server_socket);

    if(pthread_create(&cl[i].thread_id, NULL, (void *)client_handle, &cl[i].client_socket)!=0)
  {
    printf("thread creating failed\n");
  }*/

  while(1)
  {

    addr_size = sizeof server_storage;
    // new client new socket new thread
    cl[i].client_socket = accept(server_socket, (struct sockaddr *) &server_storage, &addr_size);
    cl[i].thread_id = i;

    printf("accepted connection %d\n", server_socket);

    if(pthread_create(&cl[i].thread_id, NULL, (void *)client_handle, &cl[i].client_socket)!=0)
  {
    printf("thread creating failed\n");
  }

  if(i>MAX_CLIENTS)
  {
    i =0;
    while(i<MAX_CLIENTS)
      pthread_join(cl[i++].thread_id,NULL);
    i =0;
  }


  } 
  return 0;
}


void *client_handle(void* args)
{
  printf("thread started \n");
  int socket = *((int* )args);
  char msg[1024];
  char file_name[20];


  int buf_size;
  
  buf_size = recv(socket, &file_name, sizeof(file_name), 0);
  if(buf_size <0)
    printf("not recieving file name\n");

  printf("file name: %s\n",file_name );
  
  FILE *fp = fopen(file_name, "w");
  if(!fp)
    printf("\n File not opening");


  while(buf_size = recv(socket,&msg,sizeof(msg),0) > 0)
  {
    fwrite(msg,1,buf_size, fp);
  }

  printf("%s trasnfer completed!\nn",file_name);

  close(socket);
  fclose(fp);
  pthread_exit(NULL);

}
