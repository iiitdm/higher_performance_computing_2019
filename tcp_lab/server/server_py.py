import threading
import socket

print_lock = threading.Lock()

def client_handle(client_socket, addr, i):
    """
    function to handle clients
    """
    print_lock.acquire()
    print("in thread " + str(i))
    print_lock.release()

    file_name = client_socket.recv(100)

    if not file_name:
        print_lock.acquire()
        print("file name transfer failed")
        print_lock.release()
        return

    fp = open(file_name, "w")
    data = client_socket.recv(1024).decode('ascii')
    while True:
        fp.write(data)
        #print(data)
        data = client_socket.recv(1024).decode('ascii')
        if '###' in data:
            break

    print_lock.acquire()
    print("file transfer done\n closing thread")
    print_lock.release()

    msg = "file recieved successfully"
    client_socket.send(msg.encode('ascii'))
    fp.close()
    client_socket.close()
    return








def main():

    host=""

    port = 12345

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host,port))

    print("socket binded to port", port)

    server_socket.listen(50)
    print("socket is listening")

    threads = []
    i=0
    while True:
        client_socket, addr = server_socket.accept()
        threads.append(threading.Thread(target=client_handle,
                                        args=(client_socket, addr, i)))
        threads[-1].start()
        i+=1

    while len(threads) != 0:
        t = threads.pop()
        t.join()
    server_socket.close()



if __name__ == '__main__':
    main()
