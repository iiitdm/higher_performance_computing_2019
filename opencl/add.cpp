#include <iostream>
#include <chrono>

#include <CL/cl.hpp>

int gsize = 256;
int A[256];
int B[256];

int C[256];


int main()
{

  std::vector<cl::Platform> all_platforms;
  
  // list of supported platforms
  cl::Platform::get(&all_platforms);

  if(all_platforms.size() == 0)
  {
    std::cout << "No platforms found check the installation"<<"\n";
    exit(1);
  }


  cl::Platform default_platform = all_platforms[0];
  std::cout << "Using platform" << default_platform.getInfo<CL_PLATFORM_NAME>() <<"\n";

  std::vector<cl::Device> all_devices;
  default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
  if(all_devices.size() == 0 ) {
 std:: cout << "No devices found check the installation"<<"\n";
    exit(1);
  }


  cl::Device default_device = all_devices[0];
  std::cout<< "Using devices :" << default_device.getInfo<CL_DEVICE_NAME>()<<"\n";

  cl::Context context({default_device});
  cl::Program::Sources sources;


  std::string kernel_code=
    "void kernel simple_add(global const int* A, global const int* B, global int* C) {"
    "size_t gid = get_global_id(0);"
    "C[gid] = A[gid] + B[gid];}";

  sources.push_back({ kernel_code.c_str(), kernel_code.length() });


  cl::Program program(context, sources);

  if(program.build({ default_device}) != CL_SUCCESS) {
    std::cout << "Error building:" << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device) << "\n";
  }

    cl::Buffer buffer_A(context, CL_MEM_READ_ONLY, sizeof(int) * gsize);
    cl::Buffer buffer_B(context, CL_MEM_READ_ONLY, sizeof(int) * gsize);
    cl::Buffer buffer_C(context, CL_MEM_WRITE_ONLY, sizeof(int) * gsize);

    for (int i =0 ; i<gsize; i++)
    {
      A[i] = 2;
      B[i] = 3;
    }


    cl:: CommandQueue queue(context, default_device, CL_QUEUE_PROFILING_ENABLE);


    queue.enqueueWriteBuffer(buffer_A, CL_TRUE, 0, sizeof(int) * gsize, A);
    queue.enqueueWriteBuffer(buffer_B, CL_TRUE, 0, sizeof(int) * gsize, B);

    cl::Kernel simple_add(program, "simple_add");
    simple_add.setArg(0, buffer_A);
    simple_add.setArg(0, buffer_B);
    simple_add.setArg(0, buffer_C);

    queue.enqueueNDRangeKernel(simple_add, cl::NullRange, cl::NDRange(gsize), cl::NDRange(64), NULL, NULL);


    queue.enqueueReadBuffer(buffer_C, CL_TRUE, 0, sizeof(int) *gsize, C, NULL, NULL);


    std::cout << "result"<<"\n";

    for(int i = 0; i<gsize; i++)
    {
      std::cout<< C[i] << " ";
    }

    return 0 ;

  }

