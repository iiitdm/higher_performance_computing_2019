#include<stdio.h>
#include "macro.h"
#include<mpi.h>
#include<math.h>
#include<stdlib.h>


int main(int argc, char** argv)
{

    int id, p, size;
    int low_value, high_value;
    double  elapsed_time;
    int first;
    int prime;
    int index;
    int global_count;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Barrier(MPI_COMM_WORLD);

    MPI_Comm_rank (MPI_COMM_WORLD, &id);
    MPI_Comm_size (MPI_COMM_WORLD, &p);

    if(argc != 2)
    {
        if(!id) printf("Command Line: %s <m>]n", argv[0]);
        MPI_Finalize();
        exit(1);
    }
    int n  = atoi(argv[1]);
    int i;
    low_value = 2 + BLOCK_LOW(id, p, n-1);
    high_value = 1 +BLOCK_HIGH(id, p, n-1);
    size = BLOCK_SIZE(id, p, n-1);

    printf("\n This is the number of processors:%d\n",p);
    int proc0_size = (n-1)/p;
    if((2 + proc0_size) < (int) sqrt((double) n))
        {
            if (!id) printf("Too Many processes\n");
            MPI_Finalize();
            exit(1);
        }
    char * marked = (char*) malloc(size);
    if(marked == NULL)
    {
        printf("Cannot allocate enough mem\n");
        MPI_Finalize();
        exit(1);
    }

    for(i= 0; i<size ; i++) marked[i] = 0;

    if(!id) index = 0;

    prime = 2;


    //
    do{
        if(prime*prime > low_value)
            first = prime*prime - low_value;
        else {
            if (!(low_value % prime))
            first = 0;
            else
            {
                first = prime - (low_value % prime);
            }
            
        }

        for(i = first; i<size; i+= prime)
        {
            marked[i]= 1;
        }

        if(!id)
        {
            while(marked[++index]);
            prime = index + 2;
        }

        MPI_Bcast(&prime, 1, MPI_INT, 0, MPI_COMM_WORLD);
    }
    while(prime*prime <= n);

    int count = 0;
    for(i = 0; i< size ; i++)
    {
        if(!marked[i])
        {
        printf(" %d\n", i+2);    
        count++;
        } 
    }

    MPI_Reduce (&count, &global_count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    MPI_Finalize();
    printf("number of primes in first %d numbers %d",n,global_count);
    // elapsed
    return 0;
}