#ifndef MACRO

#define MACRO

#define MIN(a,b)    ((a)<(b)? (a):(b))
#define MAX(a,b)    ((a)>(b)? (a):(b))

#define BLOCK_LOW(id, p, n)   \
                 ((id)*(n-1)/(p))

#define BLOCK_HIGH(id, p, n)   \
                 (BLOCK_LOW((id)+1, p, n) - 1)

#define BLOCK_SIZE(id, p, n)  \
                 (BLOCK_LOW((id)+1, p, n)- \
                  BLOCK_LOW((id), p, n) + 1)


#define BLOCK_OWNER(index, p, n)    \
                    (((p) * (index) + 1) - 1/(n))



#endif